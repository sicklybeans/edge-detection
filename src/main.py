import sys
from os import path
import numpy as np
import matplotlib.pyplot as plt
from progressbar import progressbar
from PIL import Image

def gaussian_blur(f, sigma=2):
  sig2 = 1.0 / (2 * sigma * sigma)
  window = int(3 *  sigma)

  h, w, _ = f.shape
  nf = np.zeros((h, w, 3))

  # prepare normalization factors
  factors = np.zeros((2 * window + 1, 2 * window + 1))
  for di in range(2 * window + 1):
    for dj in range(2 * window + 1):
      factors[di][dj] = np.exp(-((di - window)**2 + (dj - window)**2) * sig2) / (2 * 3.14159 * sigma * sigma)

  for i in progressbar(range(window, h - window)):
    for j in range(window, w - window):
      m = factors[:,:,np.newaxis] * f[i-window:i+window+1,j-window:j+window+1,:]
      nf[i][j] = np.sum(m, axis=(0,1))

  # for i in progressbar(range(h)):
  #   for j in range(w):
  #     for di in range(-window, window+1):

  #       i2 = i + di
  #       if i2 < 0:
  #         i2 = 0
  #       if i2 > h - 1:
  #         i2 = h - 1
  #       for dj in range(-window, window+1):

  #         j2 = j + dj
  #         if j2 < 0:
  #           j2 = 0
  #         if j2 > w - 1:
  #           j2 = w - 1

  #         nf[i][j][:] += factors[abs(di)][abs(dj)] * f[i2][j2][:]

  return nf

def arr_to_img(f, fname):
  h, w, _ = f.shape
  img = Image.new('RGB', (w, h))
  for i in range(h):
    for j in range(w):
      img.putpixel((j, i), (int(f[i][j][0]), int(f[i][j][1]), int(f[i][j][2])))
  img.save(fname)

def img_to_arr(f):
  img = Image.open(f)
  w, h = img.width, img.height
  f = np.zeros((h, w, 3))
  for i in range(h):
    for j in range(w):
      f[i][j][:] = img.getpixel((j, i))
  return f

def compute_derivative_matrix(f):
  h, w, _ = f.shape
  df = np.zeros((h, w , 3, 2))

  df[0,:,:,1] = (f[1,:,:] - f[0,:,:])
  df[h-1,:,:,1] = (f[h-1,:,:] - f[h-2,:,:])
  for i in range(1, h-1):
    df[i,:,:,1] = 0.5 * (f[i + 1,:,:] - f[i - 1,:,:])
  df[:,0,:,0] = (f[:,1,:] - f[:,0,:])
  df[:,w-1,:,0] = (f[:,w-1,:] - f[:,w-2,:])
  for j in range(1, w-1):
    df[:,j,:,0] = 0.5 * (f[:,j + 1,:] - f[:,j - 1,:])

  return df

def compute_derivative2_matrix(f):
  h, w, _ = f.shape
  df = np.zeros((h, w , 3, 2))
  # for i in progressbar(range(0, h)):
  #   for j in range(0, w):
  #     if i > 0 and i < h - 1:
  #       df[i,j,:,1] = 0.5 * (f[i+1][j][:] - f[i-1][j][:])
  #     elif i == 0:
  #       df[0,j,:,1] = f[1][j][:] - f[0][j][:]
  #     else:
  #       df[h - 1,j,:,1] = f[h - 1][j][:] - f[h - 2][j][:]
  #     if j > 0 and j < w - 1:
  #       df[i,j,:,0] = 0.5 * (f[i][j+1][:] - f[i][j-1][:])
  #     elif j == 0:
  #       df[i,0,:,0] = f[i][1][:] - f[i][0][:]
  #     else:
  #       df[i,w - 1,:,0] = f[i][w - 1][:] - f[i][w - 2][:]

  df[0,:,:,1] = (f[1,:,:] - f[0,:,:])
  df[h-1,:,:,1] = (f[h-1,:,:] - f[h-2,:,:])
  for i in range(1, h-1):
    df[i,:,:,1] = 0.5 * (f[i + 1,:,:] - f[i - 1,:,:])
  df[:,0,:,0] = (f[:,1,:] - f[:,0,:])
  df[:,w-1,:,0] = (f[:,w-1,:] - f[:,w-2,:])
  for j in range(1, w-1):
    df[:,j,:,0] = 0.5 * (f[:,j + 1,:] - f[:,j - 1,:])

  return df

def main(fname, blur, hessian_det_norm, output, blur_sigma=2):
  f = img_to_arr('%s' % fname)
  h, w,_ = f.shape

  if blur:
    print('Blurring input')
    oname = '%s-blurred.jpg' % output
    if path.exists(oname):
      f = img_to_arr(oname)
    else:
      f = gaussian_blur(f, sigma=blur_sigma)
      arr_to_img(f, oname)

  fig, ax = plt.subplots(1, 1, figsize=(6, 6))
  himg = Image.new('RGB', (w, h))


  ddf = np.zeros((h, w, 3, 2, 2))
  dets = np.zeros((h, w, 3))
  det_vals = []

  print('Computing first derivatives')
  df = compute_derivative_matrix(f)

  print('Computing second derivatives')
  for i in progressbar(range(1, h - 1)):
    for j in range(1, w - 1):
      ddf[i,j,:,0,0] = 0.5 * (df[i,j + 1,:,0] - df[i,j - 1,:,0])
      ddf[i,j,:,1,1] = 0.5 * (df[i + 1,j,:,1] - df[i - 1,j,:,1])
      ddf[i,j,:,0,1] = 0.5 * (df[i + 1,j,:,0] - df[i - 1,j,:,0])
      ddf[i,j,:,1,0] = 0.5 * (df[i,j + 1,:,1] - df[i,j - 1,:,1])
      det = np.abs(ddf[i,j,:,0,0]*ddf[i,j,:,1,1] - ddf[i,j,:,0,1]*ddf[i,j,:,1,0])
      for d in det:
        det_vals.append(d)
      dets[i][j][:] = det[:]

  print('Making image')
  factor = 255 / hessian_det_norm
  for i in range(1, h - 1):
    for j in range(1, w - 1):
      c = 255 - factor * dets[i][j][:]
      himg.putpixel((j, i), (int(c[0]), int(c[1]), int(c[2])))
  himg.save('%s-hessian.jpg' % output)

  ax.hist(det_vals, bins=100, range=(0, 200))
  fig.savefig('%s-det-hist.jpg' % output)

if __name__ == '__main__':
  main('input/2.jpg', False, 50, 'output/2-noblur')
  main('input/1.jpg', False, 100, 'output/1-noblur')
  main('input/1.jpg', True, 30, 'output/1-blurred-1', blur_sigma=1)
  main('input/1.jpg', True, 15, 'output/1-blurred-2', blur_sigma=2)
  main('input/1.jpg', True, 15, 'output/1-blurred-3', blur_sigma=3)
